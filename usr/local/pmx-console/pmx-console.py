import argparse
import logging
import json
import random
from tabulate import tabulate
from pmlib import Prox
from collections import OrderedDict


def generate_random_hostname():
    alphabet = ['alpha', 'bravo', 'charlie', 'delta', 'echo', 'foxtrot', 'golf', 'hotel', 'india', 'juliet', 'kilo',
                'lima', 'mike', 'november', 'oscar', 'papa', 'quebec', 'romeo', 'sierra', 'tango', 'uniform', 'victor',
                'whiskey', 'xray', 'yankee', 'zulu']
    return '{}-{}-{}'.format(alphabet[random.randint(0, len(alphabet) - 1)],
                             alphabet[random.randint(0, len(alphabet) - 1)],
                             random.randrange(00, 99))


def print_vms(data):
    print(tabulate(sorted([OrderedDict([('VMID', x.get('vmid')),
                                        ('Name', x.get('name')),
                                        ('Status', x.get('status')),
                                        ('CPU Load (%)', x.get('cpu') * 100),
                                        ('RAM Load (%)', x.get('mem') / x.get('maxmem') * 100)
                                        ]) for x in data], key=lambda k: k['Name']), headers='keys', tablefmt='simple'))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config', help='path to config, default /etc/pmx-console/config.json', default='/etc/pmx-console/config.json')
    parser.add_argument(
        '-l', '--list', help='action', choices=['nodes', 'vms', 'active', 'offline', 'templates'])
    parser.add_argument(
        '-c', '--create', nargs='?', const=1, type=str)
    parser.add_argument(
        '-t', '--template', default=None)
    parser.add_argument(
        '--cpu', choices=['1', '2', '3', '4'], default=1)
    parser.add_argument(
        '--memory', default=512)
    parser.add_argument(
        '--swap', default=512)
    parser.add_argument(
        '--size', default=4)
    parser.add_argument(
        '--stop', help='stop VM', nargs='+')
    parser.add_argument(
        '--start', help='start VM', nargs='+')
    parser.add_argument(
        '--log', help='log file. Default is None', action='store', default=None)
    parser.add_argument(
        '--log-format', help='log formatting', action='store',
        default='%(asctime)s : %(levelname)s : %(funcName)s : %(message)s')
    parser.add_argument(
        '--log-level', help='log level. Default is warning', action='store', default='error',
        choices=['debug', 'info', 'warning', 'error', 'critical'])
    args = parser.parse_args()

    loglevel = getattr(logging, args.log_level.upper())
    logging.basicConfig(
        filename=args.log, format=args.log_format, level=loglevel)

    config = {}

    try:
        config = json.load(open(args.config))
    except FileNotFoundError:
        exit('Config file not found in {}'.format(args.config))
    except json.decoder.JSONDecodeError:
        exit('JSON decode error, please check config file {}'.format(args.config))

    prox = Prox(config.get('host'),
                config.get('password'),
                config.get('username'),
                port=config.get('port'),
                verify_ssl=config.get('verify_ssl'))

    if args.list:
        if args.list == 'nodes':
            nodes = prox.get_nodes()
            print(tabulate([OrderedDict([('Name', x.get('node')),
                                        ('CPU Load (%)', x.get('cpu') * 100),
                                        ('RAM Load (%)', x.get('mem') / x.get('maxmem') * 100),
                                        ('Uptime (h)', round(x.get('uptime') / 60 / 60))
                                         ]) for x in nodes], headers='keys', tablefmt='simple'))
        if args.list == 'vms':
            vms = prox.get_vms()
            print_vms(vms)
        if args.list == 'active':
            vms = prox.get_vms('running')
            print_vms(vms)
        if args.list == 'offline':
            vms = prox.get_vms('stopped')
            print_vms(vms)
        if args.list == 'templates':
            templates = prox.get_templates()
            print(tabulate([OrderedDict([('ID', x.get('volid')),
                                         ('Size', x.get('size')/1024/1024)
                                         ]) for x in templates], headers='keys', tablefmt='simple'))

    if args.create:
        if args.create == 1:
            hostname = generate_random_hostname()
        else:
            hostname = args.create
        if args.template:
            template = args.template
        else:
            template = config.get('template')
        print('Creating new container "{}.home" from template "{}"...'.format(hostname, template))
        result = prox.create_lxc(template,
                                 ip='dhcp',
                                 gw='dhcp',
                                 hostname=hostname,
                                 ssh=''.join(config.get('ssh')),
                                 cores=args.cpu,
                                 memory=args.memory,
                                 swap=args.swap,
                                 rootfs=args.size)
        config = result.get('config')
        if config:
            logging.debug(config)
            cores = config.get('cores')
            memory = config.get('memory')
            swap = config.get('swap')
            size = config.get('rootfs')
            size = size[size.find('size=')+5:]
            print('{} core(s), {}M Memory, {}M Swap, {} Storage'.format(cores, memory, swap, size))
            print('ssh root@{}.home'.format(hostname))
        else:
            print(result)

    if args.stop:
        vms = prox.get_vms('running')
        for vm in args.stop:
            try:
                vmid = [x.get('vmid') for x in vms if x.get('name') == vm][0]
                prox.control_vm(vmid, 'stop')
            except IndexError:
                print('"{}" already stopped'.format(vm))

    if args.start:
        vms = prox.get_vms('stopped')
        for vm in args.start:
            try:
                vmid = [x.get('vmid') for x in vms if x.get('name') == vm][0]
                prox.control_vm(vmid, 'start')
            except IndexError:
                print('"{}" already running'.format(vm))
